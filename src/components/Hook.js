//importamos el hook para crear estados
//en este caso se llamaran "useState" y ""
//para ello agregar lo siguiente
//import React, { nombreHook} from "react";
import React, { useState } from "react";

import { Button } from "react-bootstrap";

export default function Hook() {
  //creamos un vector
  const [stateCar, setStateCar] = useState(false);

  const encenderApagar = () => {
    //actualizar el estado cada vez que se de click en el boton
    //ahora le decimos que obtenga el valor contrario al valor de stateCar
    //por eso ponemos !nombre
    //setStateCar(!stateCar);

    //en caso de que no tengamos acceso al estado actual y si al estado almacenado
    //utilizamos el valor del estado guardado a partir del estado actual que es prevValue
    setStateCar((prevValue) => !prevValue);
  };
  return (
    <>
      <h4>El carro está {stateCar ? "Encendido" : "Apagado"}</h4>
      <Button variant="dark" onClick={encenderApagar}>
        Encender/apagar
      </Button>
    </>
  );
}

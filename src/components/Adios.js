import React from "react";

export default function Adios() {
  //AQUI ES DONDE PODEMOS CREAR JAVASCRIPT
  //CREAR FUNCIONES

  /*const mifuncion = () => {
    console.log("Mi funcion");
  };*/

  //el return necesita encapsular todo en un solo contenido
  //es decir no podríanos devolver dos elementos
  //de igual modo, así como ponemos
  //<div>
  //</div>
  //podríanos utilizar solo llaves
  //<>
  //</>
  return (
    <>
      <h2>Adios en otro componente!</h2>
    </>
  );
}

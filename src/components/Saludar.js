import React from "react";

import { Button, Accordion, Card } from "react-bootstrap";
//ESTE VA A SER UN COMPONENTE REUTILIZABLE
//ESTO SE HACE CON LOS PROPS
export default function Saludar(props) {
  //asignacion por destructury
  //facilidad para ayudar con la legibilidad.
  const { saludarFunc, userInfo } = props;
  const { saludoConcatenado } = props;
  const { edad, nombre = "anonimo", apellido } = userInfo;

  const saludar = () => {
    alert("Hola Sin parametros");
  };

  console.log(props);
  console.log(userInfo);

  return (
    <>
      <p>
        Hola {nombre}, bienvenido! usted tiene {edad} años. color favorito{" "}
        {userInfo.color}
      </p>

      <Button variant="primary" onClick={saludar}>
        Iniciar
      </Button>
      <Button variant="secondary" onClick={() => saludarFunc(nombre)}>
        Button
      </Button>
      <Button
        variant="success"
        onClick={() => saludoConcatenado(nombre, apellido, edad)}
      >
        Concatenado
      </Button>

      <h1>Ejemplo de acordion</h1>
      <Accordion defaultActiveKey="0">
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="0">
              Click me!
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="0">
            <Card.Body>Hello!</Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card>
          <Card.Header>
            <Accordion.Toggle as={Button} variant="link" eventKey="1">
              Click me!
            </Accordion.Toggle>
          </Card.Header>
          <Accordion.Collapse eventKey="1">
            <Card.Body>Hello! I'm another body</Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </>
  );
}

//un componente siempre tiene importaciones
//importaciones como css, imagenes, etc
import React from "react";

//funcion que renderiza, la función del componente
//siempre empieza por mayúscula
//usualmente se llama igual que el fichero del componente

export default function HolaMundo() {
  //obligatoriamente tiene un return
  return (
    <div>
      <p> soy el compoente Hola Mundo </p>
      <h3> Jhordan Julcamoro </h3>
    </div>
  );
}

//un componente puede tener mas de una funcion
//pero para exportarlo solo se usa export function...
//ya que solo tiene una exportacion por default
//export
export function AdiosMundo() {
  return (
    <div>
      <p>Hasta luego componente </p>
    </div>
  );
}

//el componente siempre se tiene que exportar
//si no se exporta no se puede reutilizar
//existen dos maneras, si no se va a utilizar en otros lados
//export default HolaMundo;

//y si no se hará nada, se agrega en la parte izquierda de la funcion
//export default function HolaMundo(){
// ...
//}

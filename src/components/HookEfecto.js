import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";

export default function Hookefecto() {
  const [contar, setContar] = useState(0);

  //sirve para ejecutar cierto código después de ejecutar todo lo que queda en
  //la parte de abajo!
  //se podría utilizar para un boton de refresh
  //así traería datos de un api sin necesidad de recargar toda la página
  useEffect(
    () => {
      //todo lo que tienen aquí es lo que va a ejecutar
      console.log("Total:" + contar);
    },
    // se asignan las variables que debe detectar para que se vuelva a ejecutar
    [contar]
  );

  const clickMe = () => {
    setContar(contar + 1);
  };

  return (
    <>
      <h4>Clicks contados {contar}</h4>
      <Button variant="primary" onClick={clickMe}>
        Click me!
      </Button>
    </>
  );
}

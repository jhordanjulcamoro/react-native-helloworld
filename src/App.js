import React from "react";
import logo from "./logo.svg";
import "./App.css";

//
//PARA IMPORTAR REACT DOM NAVEGACION ENTRE P+AGINAS
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

//para importar nuestro componente creado agregamos
//import nombreComponente from "donde esta ubicado"
//import HolaMundo from "./components/HolaMundo";

//para importar las otras funciones del componente se indica de la siguiente manera
import HolaMundo, { AdiosMundo } from "./components/HolaMundo";
//importamos bootstrap para poder utilizarlo
import { Button, Navbar, Nav } from "react-bootstrap";

//importando svg imagen
//import {ReactComponent as NOMBREDEICON} form "DONDE SE UBICA"
import { ReactComponent as ReactIcon } from "./assets/svg/smartwatch.svg";

//importando componentes
import Adios from "./components/Adios";
import Saludar from "./components/Saludar";
import Hook from "./components/Hook";
import HookEfecto from "./components/HookEfecto";
import SobreNosotros from "./pages/SobreNosotros";
import Contactanos from "./pages/Contactanos";

//ENCONTRE UN PROBLEMA CON EL NAVBAR QUE RECARGA LA PÁGINA
//MIENTRAS QUE EL DIV QUE CONTIENE LOS LINKS NO,
//SOLAMENTE ACTUALIZAN LOS COMPONENTES
//EN ESTE EJERCICIO ESO ES LO QUE SE BUSCA
//MEJORAR EL TIPO DE NAVBAR EVITANDO QUE SE RECARGE LA PÁGINA COMPLETA
//SINO SOLAMENTE LOS COMPONENTES NECESARIOS
function App() {
  return (
    <div className="App">
      <Router>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand>
            <ReactIcon
              className="React-icon"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
            <Nav.Link href="/">JulHuaCorp</Nav.Link>
          </Navbar.Brand>

          <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/nosotros">Sobre nosotros</Nav.Link>
            <Nav.Link href="/contactanos">Contactanos</Nav.Link>
          </Nav>
        </Navbar>
        <div>
          <Link to="/">
            <Button>Home</Button>
          </Link>
          <Link to="/contactanos">
            <Button>Contactanos2</Button>
          </Link>
          <Link to="/nosotros">
            <Button>Sobre nosotros2</Button>
          </Link>
        </div>

        <Switch>
          <Route path="/nosotros">
            <SobreNosotros />
          </Route>
          <Route path="/contactanos">
            <Contactanos />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

/*function App() {
  const user = {
    nombre: "Jhordan",
    apellido: "Julcamoro Huaripata",
    edad: 23,
    color: "vermelho",
  };

  //recibe como parametros un nombre de un componente hijo
  const saludarFunc = (name) => {
    console.log("Hola " + name);
  };

  //recibe parametros y los concatena con `` y $
  const saludoConcatenado = (nombre, apellido, edad) => {
    console.log(`Hola ${nombre}, 
    tu apellido es ${apellido} 
    y tu edad ${edad}`);
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Soy la imagen ingresada:</h1>
        <ReactIcon className="React-icon" />
        <HolaMundo />
        <AdiosMundo />
        <Adios />
        <Button variant="danger">Danger</Button>
        <Saludar
          userInfo={user}
          saludarFunc={saludarFunc}
          saludoConcatenado={saludoConcatenado}
        />
        <h1>Agora,vamos usar HOOK do ESTADO</h1>
        <Hook />
        <h2>HOOK DO EFECTO</h2>
        <HookEfecto />
      </header>
    </div>
  );
}*/

export default App;
